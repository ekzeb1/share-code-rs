use proc_macro::TokenStream;

use proc_macro2::Span;
use quote::quote;
use syn::Data::Struct;
use syn::{parse_macro_input, Data, DataStruct, DeriveInput, Index};

#[proc_macro_derive(Device, attributes(id_field))]
pub fn device_macro_derive(input: TokenStream) -> TokenStream {
    // Construct a representation of Rust code as a syntax tree
    // that we can manipulate
    let input = parse_macro_input!(input as DeriveInput);

    proc_macro::TokenStream::from(impl_device_macro_derive(&input))
}

fn impl_device_macro_derive(ast: &DeriveInput) -> TokenStream {
    let ident = &ast.ident;

    let (impl_generics, ty_generics, where_clause) = ast.generics.split_for_impl();

    let id_method = generate_id_method(&ast.data);

    let gen = quote! {
        impl #impl_generics crate::devices::Device for #ident #ty_generics #where_clause {

            #id_method
            fn tpe(&self) -> ::std::any::TypeId { std::any::TypeId::of::<#ident>() }
            fn as_any(&self) -> &dyn std::any::Any { self }
            fn as_any_mut(&mut self) -> &mut dyn std::any::Any { self }
            fn handle_cmd(&mut self, cmd: Cmd) -> Res {
                self.handle(cmd)
            }
        }
    };

    gen.into()
}

#[rustfmt::skip]
fn generate_id_method(data: &Data) -> proc_macro2::TokenStream {
    if let Struct(DataStruct { ref fields, .. }) = data {
        fields
            .iter()
            .enumerate()
            .find(|(_, ref f)| f.attrs.iter().any(|a| a.path.is_ident("id_field")))
            .map(|(i, f)| match f.ident {
                Some(ref id) => quote! {
                    fn id(&self) -> &str { &self.#id }
                },
                _ => {
                    let idx = Index { index: i as _, span: Span::call_site(), };
                    quote! { fn id(&self) -> &str { &self.#idx} }
                }
            })
            .expect("'#[id_field]' attribute required!")
    } else {
        panic!("derive Device is implemented for structs only")
    }
}
