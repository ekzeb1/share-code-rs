use smart_home::Error;
use smart_home::*;

#[tokio::main]
async fn main() -> Result<(), Error> {
    let house = House::default();

    async_tcp::server("[::]:8080", house).await
}
