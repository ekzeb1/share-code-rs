use std::io::Write;
use std::net::{ToSocketAddrs, UdpSocket};
use std::thread::JoinHandle;

use smart_home::{udp, Error, House};

const HELP: &str = r"
send msg examples:

  $ nc -u localhost 8888
  /msg/Kitchen~Thermometer/300      // get temperature ( 3:get_cmd )                    >> 3040000 // 3:succ 0000 current temperature
  /msg/Bed Room~Thermometer/4040022 // set temperature ( 4:set_cmd 0022:i16hex=34˚C )   >> 4040022 // 4:succ 0022 current temperature
  /msg/Bed Room~Thermometer/404fff0 // set temperature ( 4:set_cmd 4fff0:i16hex=-16˚C ) >> 404fff0 ... etc
  /msg/Kitchen~Socket/300           // get power       ( 3:get_cmd )
  /msg/Kitchen~Socket/40400dc       // set power       ( 4:set_cmd 00dc:u16hex:220W )

";

const SERVER_ADDR: &str = "[::]:8888";

fn main() -> Result<(), Error> {
    let house = House::default();

    println!("{}", HELP);

    udp::str_server(SERVER_ADDR, house, [client(SERVER_ADDR)])
}

/// naive str_server CLI client
///
/// * `addr` - str_server address
fn client<A: ToSocketAddrs + Send + 'static>(addr: A) -> JoinHandle<Result<(), Error>> {
    std::thread::spawn(move || {
        fn prompt() -> std::io::Result<()> {
            print!(">> ");
            std::io::stdout().flush()
        }

        std::thread::park();

        let socket = UdpSocket::bind("[::]:0")?;

        socket.set_nonblocking(true)?;

        socket.connect(addr)?;

        let mut line_buf = String::new();
        loop {
            prompt()?;
            if let Ok(len) = std::io::stdin().read_line(&mut line_buf) {
                if len == 0 {
                    return Ok(());
                }
                let line = line_buf.trim();

                socket.send(line.as_bytes())?;

                let mut buf = [0u8; udp::DATAGRAM_SIZE];
                let len = loop {
                    match socket.recv_from(&mut buf) {
                        Ok((len, _)) => break len,
                        Err(e) if e.kind() == std::io::ErrorKind::WouldBlock => continue,
                        Err(e) => panic!("error recv {}", e),
                    }
                };

                let buf = &mut buf[..len];

                println!("<< {}", std::str::from_utf8(buf)?)
            }

            String::clear(&mut line_buf);
        }
    })
}
