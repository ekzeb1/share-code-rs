struct HasDrop;
impl Drop for HasDrop {
    fn drop(&mut self) {
        println!("Dropping HasDrop")
    }
}

struct HAsTwoDrop {
    one: HasDrop,
    two: HasDrop,
}

impl Drop for HAsTwoDrop {
    fn drop(&mut self) {
        println!("Dropping HasTwoDrop")
    }
}

fn main() -> std::io::Result<()> {
    // use std::cmp::Reverse;
    //
    // let mut v = vec![1, 2, 3, 4, 5, 6];
    // v.sort_by_key(|&num| (num > 2, Reverse(num))); // Reverse(num)
    // println!("{:?}", v);

    let _x = HAsTwoDrop {
        one: HasDrop,
        two: HasDrop,
    };

    println!("running");

    Ok(())
}
