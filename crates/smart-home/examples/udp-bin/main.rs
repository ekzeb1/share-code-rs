use smart_home::{udp, Error, House};

mod client;

fn main() -> Result<(), Error> {
    let house = House::default();

    udp::bin_server("[::]:8889", house, [client::run()])
}
