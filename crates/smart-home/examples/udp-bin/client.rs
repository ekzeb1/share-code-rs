use std::net::UdpSocket;
use std::thread::JoinHandle;
use std::time::Duration;

use rand::prelude::*;
use smart_home::devices::Cmd;
use smart_home::udp::{DeviceCmd, DeviceRes, DATAGRAM_SIZE};
use smart_home::Error::SendError;
use smart_home::*;

pub const SERVER_ADDR: &str = "[::]:8889";

fn send_receive(sock: UdpSocket, msg: DeviceCmd) -> Result<DeviceRes, Error> {
    let ser = bincode::serialize(&msg)?;

    sock.send(&ser)?;
    println!("bin_client: >> SEND {}", msg);
    let mut buf = [0u8; DATAGRAM_SIZE];
    let (len, _) = loop {
        match sock.recv_from(&mut buf) {
            Ok(l) => break l,
            Err(e) if e.kind() == std::io::ErrorKind::WouldBlock => continue,
            Err(e) => panic!("error recv {}", e),
        };
    };

    let buf = &mut buf[..len];

    bincode::deserialize(buf).map_err(|e| SendError(e.to_string()))
}

pub fn run() -> JoinHandle<Result<(), Error>> {
    std::thread::spawn(move || {
        std::thread::park();
        let socket = UdpSocket::bind("[::]:0").expect("Could not bind client socket");
        socket.set_nonblocking(true)?;
        socket.connect(SERVER_ADDR).expect("Could not connect to server");

        let set_msg = DeviceCmd::from(("Kitchen", "Thermometer"), Cmd::Gen(b'4', 32i16.to_be_bytes().to_vec()));
        let get_msg = DeviceCmd::from(("Kitchen", "Thermometer"), Cmd::Gen(b'3', vec![]));

        let mut i = 0;
        loop {
            let set_msg = if i > 0 {
                let temperature = thread_rng().gen_range(-60..60) as i16;
                println!("bin_client: >> SEND SET NEW TEMPERATURE {}", temperature);
                let temperature = temperature.to_be_bytes().to_vec();
                DeviceCmd {
                    cmd: Cmd::Gen(52, temperature),
                    ..set_msg.clone()
                }
            } else {
                set_msg.clone()
            };

            println!(
                "bin_client: << RECV SET RES {}",
                send_receive(socket.try_clone().expect("Could not clone socket"), set_msg)?
            );

            std::thread::sleep(Duration::from_secs(1));

            println!(
                "bin_client: << RECV GET RES {}",
                send_receive(socket.try_clone().expect("Could not clone socket"), get_msg.clone())?
            );

            i += 1;
            std::thread::sleep(std::time::Duration::from_secs(2));
        }
    })
}
