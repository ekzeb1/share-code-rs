use smart_home::Error;
use smart_home::*;

fn main() -> Result<(), Error> {
    let house = House::default();

    tcp::server("[::]:8080", house)
}
