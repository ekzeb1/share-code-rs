use smart_home::devices::*;
use smart_home::Error;
use smart_home::*;

fn main() -> Result<(), Error> {
    let house = House::default();

    house
        .filter(|r| r.id() == "Kitchen", |_| true)
        .for_each(|r| println!(">> {}", r));

    let socket_criteria = Socket::new("Socket");

    println!(
        "\nSockets report:\n==============\n\n{}",
        house.query_report(&socket_criteria as &dyn Device)?
    );

    let thermometer_c = Thermometer::new("Thermometer");
    println!(
        "Thermometers report:\n====================\n\n{}",
        house.query_report(&thermometer_c as &dyn Device)?
    );

    Ok(())
}
