#!/usr/bin/env bash

tar_name="${1:-lesson}"

yn="$2"

function targz() {
    rm -rf "$tar_name"_dz.tar.gz || true &&
    cargo clean &&
    tar -cvzf "$tar_name"_dz.tar.gz --exclude=.git --exclude=.idea ./
}

[[ -n "$yn" ]] && targz && exit
[[ -z "$yn" ]] && {

  echo Will compress project to \'"${tar_name}"_dz.tar.gz\' file

  while true; do

  read -r -p "Do you want to proceed? (y/n) " yn

  case $yn in
    [yY] ) echo ok, compressing...;
      targz ;
      break;;
    [nN] ) echo exiting...;
      exit;;
    * ) echo invalid response;;
  esac

  done
}
