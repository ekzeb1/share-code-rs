use tap::prelude::*;

use smart_home::devices::*;
use smart_home::{home::House, home::Room, Error};

#[test]
fn test_room_add_rm() {
    let socket = Socket::new("Розетка1");

    let mut room = Room::new("test") + socket.clone();

    assert_eq!(room.devices().count(), 1);

    assert!(room.remove("Розетка1"));

    assert_eq!(room.devices().count(), 0);

    assert!(room.add_unique(socket.clone()).is_ok());

    assert_eq!(room.add_unique(socket.clone()), Err(socket));

    assert_eq!(room.devices().count(), 1);
}

#[test]
fn test_filter_by_dyn_trait() -> Result<(), Error> {
    let room = Room::new("Kitchen") + Socket::new("Socket") + Thermometer::new("Thermometer");

    let criteria = Socket::new("Socket");
    assert_eq!(room.query(&criteria as &dyn Device).count(), 1);

    Ok(())
}

#[test]
fn test_query() {
    let room = Room::new("Kitchen") + Socket::new("Socket") + Thermometer::new("Thermometer");

    let mut bed_room = Room::new("Bed room") + Socket::new("Socket_b");
    bed_room += Thermometer::new("Thermometer_b");

    let house = House::new("Smart") + room + bed_room;

    let criteria = ("Kitchen", "Thermometer");

    let result = house.query_devices(&criteria).collect::<Vec<_>>();

    result.iter().for_each(|d| println!("{:?}", d));

    assert_eq!(result.len(), 1);
    assert_eq!(result.first().unwrap().id(), "Thermometer");
}

#[test]
fn test_home() {
    let room = Room::new("test").with_device(Socket::new("Розетка1"));
    let home = House::new("Test house").with_room(room);
    assert_eq!(home.rooms().count(), 1);
}

#[test]
fn test_turn_on_device() -> Result<(), Error> {
    let mut room = Room::new("test").with_device(Socket::new("Розетка1"));

    room.call(("Розетка1",), |d: &mut Socket| d.on())?;

    let socket_criteria = Socket::new("Розетка1");

    let maybe_socket = room
        .query(&socket_criteria as &dyn Device)
        .next()
        .and_then(|d| d.as_any().downcast_ref::<Socket>());

    assert!(maybe_socket.is_some());

    assert_eq!(maybe_socket.unwrap().current_state(), SocketState::On);

    let socket_criteria = Socket::new("Розетка1");

    let maybe_socket = room
        .find(&socket_criteria)
        .and_then(|d| d.as_any().downcast_ref::<Socket>());

    assert!(maybe_socket.is_none());

    let socket_criteria = Socket::new("Розетка1").tap_mut(|s| s.on());

    let maybe_socket = room
        .find(&socket_criteria)
        .and_then(|d| d.as_any().downcast_ref::<Socket>());

    assert!(maybe_socket.is_some());

    Ok(())
}

#[test]
fn test_msg_serde() -> Result<(), Error> {
    let msg = Cmd::Gen(51, vec![53, 64]);

    let bites = bincode::serialize(&msg)?;
    let round: Cmd = bincode::deserialize(&bites)?;

    println!("<<< {:?} {}", round, round);

    assert_eq!(round, msg);

    Ok(())
}
