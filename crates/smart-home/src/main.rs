use smart_home::devices::*;
use smart_home::*;

fn main() -> Result<(), Error> {
    let mut house = House::new("House")
        .with_room(
            Room::new("Kitchen")
                .with_device(Socket::new("Socket"))
                .with_device(Thermometer::new("Thermometer"))
                .with_device(Thermometer::new("Thermometer2")),
        )
        .with_room(
            Room::new("Bed Room")
                .with_device(Thermometer::new("Thermometer"))
                .with_device(Socket::new("Socket")),
        )
        .with_room(Room::new("Сloset"));
    println!("\n{}", house);

    house.call(("Kitchen", "Socket"), |d: &mut Socket| d.on())?;

    println!("\n{}", house);

    let mut house = House::new("House2") + Room::new("test").with_device(Socket::new("Socket"));

    house += Room::new("test2").with_device(Thermometer::new("Thre344"));

    println!("{}", house);

    Ok(())
}
