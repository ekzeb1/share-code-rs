use crate::devices::{Id, Res};
use crate::model::devices::Cmd::{self, *};
use crate::{Error, ParseError};
use serde::{Deserialize, Serialize};
use std::borrow::Borrow;
use std::fmt::{Display, Formatter, Write};
use std::str::FromStr;

#[derive(Debug, Default, PartialEq, Eq, Deserialize, Serialize, Clone)]
pub struct DeviceCmd {
    pub id: Id,
    pub cmd: Cmd,
}

impl DeviceCmd {
    pub fn ids(&self) -> (&str, &str) {
        (self.id.room_id(), self.id.device_id())
    }

    pub fn from(ids: (&str, &str), msg: Cmd) -> Self {
        Self {
            id: Id::new(ids.0, ids.1),
            cmd: msg,
        }
    }
}

impl FromStr for DeviceCmd {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s
            .trim()
            .split('/')
            .filter_map(|s| match s.trim() {
                t if t.is_empty() => None,
                t => Some(t),
            })
            .collect::<Vec<_>>()
            .as_slice()
        {
            ["msg", id, cmd] => id
                .parse::<Id>()
                .and_then(|id| cmd.parse::<Cmd>().map(|cmd| DeviceCmd { id, cmd })),
            ufo => Err(Error::ParseError(format!("Invalid DeviceCmd format: '{}'", s))),
        }
    }
}

impl Display for DeviceCmd {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!("/msg/{}/{}", self.id, self.cmd))
    }
}

#[derive(Deserialize, Serialize, Clone, Debug)]
pub struct DeviceRes {
    pub(in crate::network) res: Res,
}

impl DeviceRes {
    pub fn err(s: String) -> Self {
        Self { res: Res::Err(s) }
    }
    pub fn ok(res: Res) -> Self {
        Self { res }
    }
}

impl Display for DeviceRes {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!("{}", self.res))
    }
}

#[cfg(test)]
mod tests {

    use super::*;

    #[test]
    fn test_dev_smd_serde() {
        let noop = DeviceCmd {
            id: Id::new("Kitchen", "Socket"),
            cmd: Noop,
        };

        assert_eq!(noop, "/msg/Kitchen~Socket/0".parse::<DeviceCmd>().unwrap());
        assert_eq!("/msg/Kitchen~Socket/0", noop.to_string());

        let get_msq = DeviceCmd {
            id: Id::new("Kitchen", "Socket"),
            cmd: Gen(51, vec![]),
        };

        assert_eq!(get_msq, "/msg/Kitchen~Socket/300".parse::<DeviceCmd>().unwrap());
        assert_eq!("/msg/Kitchen~Socket/300", get_msq.to_string());

        let set_msg = DeviceCmd {
            id: Id::new("Kitchen Z", "Socket"),
            cmd: Gen(52, vec![0, 34]),
        };

        println!("set_msg {set_msg}");

        assert_eq!(set_msg, "/msg/Kitchen Z~Socket/4040022".parse::<DeviceCmd>().unwrap());
        assert_eq!("/msg/Kitchen Z~Socket/4040022", set_msg.to_string());

        let enc = bincode::serialize(&get_msq).unwrap();
        let dec = bincode::deserialize(&enc[..]).unwrap();

        assert_eq!(get_msq, dec)
    }
}
