mod protocol;
mod socket;

use crate::devices::Res;
use crate::misc::thread_pool::*;
use crate::{Error, House, ServerError};
pub use protocol::*;
pub use socket::*;
use std::io::Write;
use std::net::{SocketAddr, ToSocketAddrs, UdpSocket};
use std::sync::{Arc, Mutex};
use std::thread::JoinHandle;

pub const DATAGRAM_SIZE: usize = (1 << 8) + 2; // 258b

/// string datagrams read/write server
///
/// * `addr`  - server address
/// * `house` - smart home object
pub fn str_server<A: ToSocketAddrs + Send + 'static, const N: usize>(
    addr: A,
    house: House,
    clients: [JoinHandle<Result<(), Error>>; N],
) -> Result<(), Error> {
    let house = Arc::new(Mutex::new(house));

    std::thread::spawn(move || {
        let mut server = ServerSocket::new(addr).expect("Could not connect");
        let threads = ThreadPool::new();

        clients.into_iter().for_each(|c| c.thread().unpark());

        loop {
            match server.recv_from() {
                Ok(addr) => {
                    let socket = server.clone();
                    let house = Arc::clone(&house);
                    threads.run(move || {
                        str_handle(addr, socket, house);
                    });
                }
                Err(ref err) if would_block(err) => {
                    // wait until network socket is ready
                    continue;
                }
                Err(e) => {
                    eprintln!("{}", e);
                    continue;
                }
            }
        }
    })
    .join()
    .map_err(|_| ServerError(std::io::Error::last_os_error()))
}

/// string datagrams server handle
///
/// * `msg`    - parsed msg from client
/// * `addr`   - client socket address
/// * `socket` - server socket
/// * `house`  - smart home object
fn str_handle(addr: SocketAddr, mut socket: ServerSocket, house: Arc<Mutex<House>>) -> Result<usize, Error> {
    let result = match socket.read_str_msg() {
        Ok(DeviceCmd { id, cmd }) => {
            let mut house = house.lock().unwrap();
            house.send_cmd(id, cmd)
        }
        Err(e) => {
            eprintln!("bin_server: >> SEND ERR {}", e);
            return socket.write_as_str(DeviceRes::err(e.to_string()), &addr);
        }
    };

    match result {
        Ok(res @ Res::Ok(..)) => socket.write_as_str(DeviceRes::ok(res), &addr),
        Ok(Res::Err(er)) => socket.write_as_str(DeviceRes::err(er), &addr),
        Err(e) => socket.write_as_str(DeviceRes::err(e.to_string()), &addr),
    }
}

/// bincode datagrams read/write server
///
/// * `addr`  - server address
/// * `house` - smart home object
pub fn bin_server<A: ToSocketAddrs + Send + 'static, const N: usize>(
    addr: A,
    house: House,
    clients: [JoinHandle<Result<(), Error>>; N],
) -> Result<(), Error> {
    let house = Arc::new(Mutex::new(house));

    std::thread::spawn(move || {
        let mut server = ServerSocket::new(addr).expect("Could not connect");
        let threads = ThreadPool::new();

        clients.into_iter().for_each(|c| c.thread().unpark());

        loop {
            match server.recv_from() {
                Ok(addr) => {
                    let socket = server.clone();
                    let house = Arc::clone(&house);
                    threads.run(move || {
                        bin_handle(addr, socket, house);
                    });
                }
                Err(ref err) if would_block(err) => {
                    // wait until network socket is ready
                    continue;
                }
                Err(e) => {
                    eprintln!("bin_server: ERR {}", e);
                    continue;
                }
            }
        }
    })
    .join()
    .map_err(|_| ServerError(std::io::Error::last_os_error()))
}

/// bincode datagrams server handle
///
/// * `msg`    - parsed msg from client
/// * `addr`   - client socket address
/// * `socket` - server socket
/// * `house`  - smart home object
fn bin_handle(addr: SocketAddr, mut socket: ServerSocket, house: Arc<Mutex<House>>) -> Result<usize, Error> {
    let result = match socket.read_bin_msg() {
        Ok(DeviceCmd { id, cmd }) => {
            let mut house = house.lock().unwrap();
            house.send_cmd(id, cmd)
        }
        Err(e) => {
            eprintln!("bin_server: >> SEND ERR {}", e);
            return socket.write_as_bin(DeviceRes::err(e.to_string()), &addr);
        }
    };

    // let result = {
    //     let mut house = house.lock().unwrap();
    //     if let DeviceMsg {id, cmd} = msg {
    //         house.send_msg(id, cmd)
    //     } else { Err(Error::SendError("".to_string())) }
    //
    // };

    match result {
        Ok(res @ Res::Ok(..)) => {
            println!("bin_server: >> SEND RES {}", res);
            socket.write_as_bin(DeviceRes::ok(res), &addr)
        }
        Ok(Res::Err(er)) => {
            eprintln!("bin_server: >> SEND RES_ERR {}", er);
            socket.write_as_bin(DeviceRes::err(er), &addr)
        }
        Err(e) => {
            eprintln!("bin_server: >> SEND ERR {}", e);
            socket.write_as_bin(DeviceRes::err(e.to_string()), &addr)
        }
    }
}

fn would_block(err: &Error) -> bool {
    if let Error::ServerError(e) = err {
        e.kind() == std::io::ErrorKind::WouldBlock
    } else {
        false
    }
}
