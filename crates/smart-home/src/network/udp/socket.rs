use super::protocol::*;
use crate::devices::Res;
use crate::udp::DATAGRAM_SIZE;
use crate::{Error, ParseError, ServerError};
use std::io::{BufRead, BufReader, Read, Write};
use std::net::{SocketAddr, ToSocketAddrs, UdpSocket};

/// Bytes read/write in [`UdpSocket`] wrapper
struct BufSocket {
    udp_socket: UdpSocket,
    datagram: Vec<u8>,
    buf: Vec<u8>,
}

impl Clone for BufSocket {
    fn clone(&self) -> Self {
        BufSocket {
            udp_socket: match self.udp_socket.try_clone() {
                Ok(s) => s,
                Err(e) => panic!("unable clone UdpSocket {}", e),
            },
            datagram: Default::default(),
            buf: self.buf.clone(),
        }
    }
}

impl BufSocket {
    /// Create non_blocking [`UdpSocket`]
    ///
    /// * `addr` - server socket address
    fn new<Addr: ToSocketAddrs>(addr: Addr) -> Result<BufSocket, Error> {
        let udp_socket = UdpSocket::bind(addr)?;
        udp_socket.set_nonblocking(true)?;
        Ok(BufSocket {
            udp_socket,
            datagram: vec![0u8; DATAGRAM_SIZE],
            buf: Default::default(),
        })
    }

    /// Read raw bytes datagram into
    fn read_buf(&mut self) -> Result<SocketAddr, Error> {
        let (c, a) = self.udp_socket.recv_from(&mut self.datagram)?;
        self.buf = self.datagram[..c].into();
        Ok(a)
    }

    /// Write bytes slice to client side `Socket::write` wrapper
    ///
    /// * `addr` - client socket address
    /// * `buf`  - bytes to write
    fn write_buf(&self, buf: &[u8], addr: &SocketAddr) -> Result<usize, Error> {
        let r = self.udp_socket.send_to(buf, addr)?;
        Ok(r)
    }
}

#[derive(Clone)]
pub struct ServerSocket {
    socket: BufSocket,
}

impl ServerSocket {
    /// Create server side udp socket
    ///
    /// * `addr` - server address
    pub fn new<Addr: ToSocketAddrs>(addr: Addr) -> Result<ServerSocket, Error> {
        let socket = BufSocket::new(addr)?;
        Ok(ServerSocket { socket })
    }

    /// Read [`DeviceMsg`] from string datagram
    pub fn read_str_msg(&mut self) -> Result<DeviceCmd, Error> {
        match std::str::from_utf8(&self.socket.buf)
            .map_err(|e| e.into())
            .and_then(|s| s.parse::<DeviceCmd>())
        {
            Ok(command) => Ok(command),
            Err(e) => Err(e),
        }
    }

    /// Read [`DeviceMsg`] from bincode datagram
    pub fn read_bin_msg(&mut self) -> Result<DeviceCmd, Error> {
        let command = bincode::deserialize(&self.socket.buf)?;
        Ok(command)
    }

    /// Read raw bytes datagram
    pub fn recv_from(&mut self) -> Result<SocketAddr, Error> {
        self.socket.read_buf()
    }

    /// Write [`DeviceRes`] as string datagram
    ///
    /// * `addr` - client socket address
    /// * `res`  - device response
    pub fn write_as_str(&self, res: DeviceRes, addr: &SocketAddr) -> Result<usize, Error> {
        let bytes = res.to_string() + "\n";
        self.write_buf(bytes.as_bytes(), addr)
    }

    /// Write [`DeviceRes`] as bincode datagram
    ///
    /// * `addr` - client socket address
    /// * `res`  - device response
    pub fn write_as_bin(&self, res: DeviceRes, addr: &SocketAddr) -> Result<usize, Error> {
        let bytes = bincode::serialize(&res)?;
        self.write_buf(&bytes, addr)
    }

    /// Write bytes slice to client side `Socket::write` wrapper
    ///
    /// * `addr` - client socket address
    /// * `buf`  - bytes to write
    pub fn write_buf(&self, buf: &[u8], addr: &SocketAddr) -> Result<usize, Error> {
        self.socket.write_buf(buf, addr)
    }
}
