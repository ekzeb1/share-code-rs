use std::sync::{Arc, RwLock};

use tokio::io::{AsyncBufReadExt, AsyncWriteExt, BufReader};
use tokio::net::{TcpListener, TcpStream, ToSocketAddrs};

use crate::Error::ServerError;
use crate::{Error, House, SendError};

use super::tcp::protocol::Req::*;
use super::tcp::protocol::Res;

pub async fn server<A: ToSocketAddrs>(addr: A, house: House) -> Result<(), crate::Error> {
    let mut listener = TcpListener::bind(addr).await?;

    let mut house = Arc::new(RwLock::new(house));

    while let Ok((stream, remote)) = listener.accept().await {
        println!("accept new client[{}]", remote);
        let house = Arc::clone(&house);
        tokio::spawn(handle(stream, house));
    }

    Ok(())
}

async fn handle(mut stream: TcpStream, house: Arc<RwLock<House>>) -> Result<(), crate::Error> {
    let (read, mut write) = stream.split();
    let mut buf = BufReader::new(read);

    let mut handle_req = |line: &str| -> Result<Res, Error> {
        match line.try_into()? {
            ModifySocket { room_id, device_id, f } => {
                let mut house = house.write().unwrap();
                house.call((room_id, device_id), f).map(|_| Res::Ok)
            }
            ModifyThermometer { room_id, device_id, f } => {
                let mut house = house.write().unwrap();
                house.call((room_id, device_id), f).map(|_| Res::Ok)
            }
            ModifyDevice { room_id, device_id, f } => {
                let mut house = house.write().unwrap();
                house.call_dyn((room_id, device_id), f).map(|_| Res::Ok)
            }
            Report { room_id, device_id } => {
                let mut house = house.read().unwrap();
                match device_id {
                    Some(device_id) => house.query_report(&(room_id, device_id)).map(Res::Report),
                    _ => house.query_report(&(room_id.as_str(),)).map(Res::Report),
                }
            }
        }
    };

    loop {
        let mut line = String::new();

        match buf.read_line(&mut line).await {
            Ok(n) if n <= 0 => break Ok(()),
            Err(e) => {
                eprintln!("error read Req from line: {}", e);
                write.write_all(Res::Err(e.into()).to_string().as_bytes()).await?;
                continue;
            }
            _ => {}
        };

        let mut res = async {
            match handle_req(&line) {
                Ok(res) => res,
                Err(e) => {
                    eprintln!("error handle Req: {}", e);
                    Res::Err(e)
                }
            }
        }
        .await;

        match write.write_all(res.to_string().as_bytes()).await {
            Err(e) => {
                eprintln!("error write Res: {}", e);
                break write
                    .write_all(Res::Err(e.into()).to_string().as_bytes())
                    .await
                    .map_err(ServerError);
            }
            _ => continue,
        }
    }
}
