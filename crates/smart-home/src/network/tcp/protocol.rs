#![allow(clippy::type_complexity)]

use crate::devices::{Device, Socket, Thermometer};
use crate::tcp::protocol::Req::*;
use crate::Error;
use crate::Error::UnknownReq;
use derivative::Derivative;
use std::any::Any;
use std::fmt::{Debug, Display, Formatter, Write};

#[derive(Derivative)]
#[derivative(PartialEq, Eq, Debug)]
pub(in crate::network) enum Req {
    ModifySocket {
        room_id: String,
        device_id: String,
        #[derivative(PartialEq = "ignore")]
        #[derivative(Debug = "ignore")]
        f: Box<dyn FnMut(&mut Socket)>,
    },
    ModifyThermometer {
        room_id: String,
        device_id: String,
        #[derivative(PartialEq = "ignore")]
        #[derivative(Debug = "ignore")]
        f: Box<dyn FnMut(&mut Thermometer)>,
    },
    ModifyDevice {
        room_id: String,
        device_id: String,
        #[derivative(PartialEq = "ignore")]
        #[derivative(Debug = "ignore")]
        f: Box<dyn FnMut(&mut dyn Device)>,
    },
    Report {
        room_id: String,
        device_id: Option<String>,
    },
}

impl TryFrom<&str> for Req {
    type Error = crate::Error;

    fn try_from(line: &str) -> Result<Self, Self::Error> {
        let parts = line
            .trim()
            .split('/')
            .filter_map(|l| match l.trim() {
                "" => None,
                line => Some(line),
            })
            .collect::<Vec<_>>();

        match parts.as_slice() {
            ["socket", room_id, device_id, cmd] => Ok(ModifySocket {
                room_id: room_id.to_string(),
                device_id: device_id.to_string(),
                f: if *cmd == "on" {
                    Box::new(|s| s.on())
                } else if *cmd == "off" {
                    Box::new(|s| s.off())
                } else {
                    let pow = cmd.parse::<u16>().map_err(|e| Error::ParseError(e.to_string()))?;
                    Box::new(move |s| s.set_power(pow))
                },
            }),

            ["thermometer", room_id, device_id, cmd] => Ok(ModifyThermometer {
                room_id: room_id.to_string(),
                device_id: device_id.to_string(),
                f: if *cmd == "on" {
                    Box::new(|s| s.on())
                } else if *cmd == "off" {
                    Box::new(|s| s.off())
                } else {
                    let temp = cmd.parse::<i16>().map_err(|e| Error::ParseError(e.to_string()))?;
                    Box::new(move |s| s.set_temperature(temp))
                },
            }),
            ["device", room_id, device_id, cmd] => {
                if *cmd == "on" {
                    Ok(ModifyDevice {
                        room_id: room_id.to_string(),
                        device_id: device_id.to_string(),
                        f: Box::new(|s: &mut dyn Device| s.on()),
                    })
                } else if *cmd == "off" {
                    Ok(ModifyDevice {
                        room_id: room_id.to_string(),
                        device_id: device_id.to_string(),
                        f: Box::new(|s: &mut dyn Device| s.off()),
                    })
                } else {
                    Err(Error::ParseError(format!(
                        "Unknown cmd for device {} {}",
                        room_id, device_id
                    )))
                }
            }
            ["report", room_id, res @ ..] => Ok(Report {
                room_id: room_id.to_string(),
                device_id: res.first().map(|id| id.to_string()),
            }),
            [ufo, ..] => Err(UnknownReq(ufo.to_string())),
            _ => Err(UnknownReq(String::from("empty"))),
        }
    }
}

#[derive(Debug, Default)]
pub(in crate::network) enum Res {
    #[default]
    Ok,
    Err(Error),
    Report(String),
}

impl Display for Res {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Res::Ok => f.write_str("+OK\r\n"),
            Res::Err(err) => f.write_fmt(format_args!("-{}\r\n", err)),
            Res::Report(v) => f.write_fmt(format_args!("+OK\r\n${}\r\n{}\r\n", v.len(), v)),
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::devices::{Device, Socket};
    use crate::tcp::protocol::Req;

    #[test]
    fn test_req_from_str() {
        let turn_off: Req = "/thermometer/room/device/off".try_into().unwrap();
        assert_eq!(
            turn_off,
            Req::ModifyThermometer {
                room_id: "room".to_string(),
                device_id: "device".to_string(),
                f: Box::new(|s| s.off()),
            }
        );

        let turn_on: Req = "/socket/room/device/off".try_into().unwrap();
        assert_eq!(
            turn_on,
            Req::ModifySocket {
                room_id: "room".to_string(),
                device_id: "device".to_string(),
                f: Box::new(|s| s.off()),
            }
        );

        let report: Req = "/report/room/device".try_into().unwrap();
        assert_eq!(
            report,
            Req::Report {
                room_id: "room".to_string(),
                device_id: Some("device".to_string())
            }
        );

        let report: Req = "/report/room".try_into().unwrap();
        assert_eq!(
            report,
            Req::Report {
                room_id: "room".to_string(),
                device_id: None
            }
        )
    }
}
