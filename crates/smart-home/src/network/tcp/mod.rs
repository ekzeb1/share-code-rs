use std::io::{BufRead, BufReader, Write};
use std::net::{TcpListener, TcpStream, ToSocketAddrs};
use std::sync::{Arc, RwLock};

use crate::devices::Device;
use crate::tcp::protocol::{Req, Res};
use crate::Error::ServerError;
use crate::{Error, House};

pub(in crate::network) mod protocol;

pub fn server<A: ToSocketAddrs>(addr: A, house: House) -> Result<(), crate::Error> {
    let mut listener = TcpListener::bind(addr)?;

    let mut house = Arc::new(RwLock::new(house));

    while let Ok((stream, remote)) = listener.accept() {
        println!("accept new client[{}]", remote);
        let house = Arc::clone(&house);
        std::thread::spawn(move || handle(stream, house));
    }

    Ok(())
}

fn handle(mut stream: TcpStream, house: Arc<RwLock<House>>) -> Result<(), crate::Error> {
    use Req::*;
    let mut buf = BufReader::new(stream.try_clone()?);
    let mut line = String::new();

    loop {
        buf.read_line(&mut line).map_err(ServerError).and_then(|_| {
            match line.as_str().try_into()? {
                ModifySocket { room_id, device_id, f } => {
                    let mut house = house.write().unwrap();
                    house.call((room_id, device_id), f).map(|_| Res::Ok)
                }
                ModifyThermometer { room_id, device_id, f } => {
                    let mut house = house.write().unwrap();
                    house.call((room_id, device_id), f).map(|_| Res::Ok)
                }
                ModifyDevice { room_id, device_id, f } => {
                    let mut house = house.write().unwrap();
                    house.call_dyn((room_id, device_id), f).map(|_| Res::Ok)
                }
                Report { room_id, device_id } => {
                    let mut house = house.read().unwrap();
                    match device_id {
                        Some(device_id) => house.query_report(&(room_id, device_id)).map(Res::Report),
                        _ => house.query_report(&(room_id.as_str(),)).map(Res::Report),
                    }
                }
            }
            .and_then(|res| {
                stream
                    .write_all(res.to_string().as_bytes())
                    .map_err(Error::ServerError)
                    .map(|_| line.clear())
            })
        });
    }
}
