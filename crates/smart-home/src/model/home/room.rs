use std::any::Any;
use std::borrow::Borrow;
use std::fmt::{format, Debug, Display, Formatter, Pointer};
use std::ops::{Add, AddAssign, Deref, DerefMut, Index, IndexMut, Sub, SubAssign};
use std::rc::Rc;
use std::sync::mpsc::Sender;

use crate::devices::*;
use crate::devices::{CmdHandler, CmdResult};
use crate::Error::{NoDevices, NoRooms, NoSuchDevice, NoSuchRoom};
use crate::{devices, Error, Query, SendError};

pub type RoomId<'a> = &'a str;
pub type DeviceId<'a> = &'a str;

#[derive(Debug, PartialEq, Eq)]
pub struct Room {
    name: String,
    pub(in crate::model) devices: Vec<Box<dyn Device>>,
}

impl Room {
    /// Creates room with
    ///
    /// * `name` - room name
    pub fn new(name: &str) -> Room {
        Room {
            name: name.to_string(),
            devices: Default::default(),
        }
    }

    pub fn id(&self) -> &str {
        &self.name
    }

    pub fn with_device<T: Device + 'static>(mut self, device: T) -> Room {
        match self.add_unique(device) {
            Ok(_) => self,
            Err(d) => panic!("{}", Error::NotUniqueDeviceId(d.id().to_string())),
        }
    }

    pub fn add_unique<D: Device + 'static>(&mut self, device: D) -> Result<(), D> {
        if self.devices().any(|d| d.id() == device.id()) {
            Err(device)
        } else {
            self.devices.push(Box::new(device));
            Ok(())
        }
    }

    pub fn devices(&self) -> impl Iterator<Item = &Box<dyn Device>> + '_ {
        self.devices.iter()
    }

    pub fn query<'a, Q: Query + ?Sized>(&'a self, q: &'a Q) -> impl Iterator<Item = &Box<dyn Device>> + 'a {
        self.devices().filter(move |&d| q.eq_device(d.deref()))
    }

    /// Filter devices
    ///
    /// Filter devices by comparing concrete type instance using [`PartialEq`] eq
    ///
    /// * `criteria` - T concrete [`Device`] implementation
    pub fn find<T>(&self, criteria: &T) -> Option<&dyn Device>
    where
        T: Device + Any + PartialEq + Eq,
    {
        self.devices()
            .find(move |d| d.as_any().downcast_ref::<T>().map_or(false, |v| v == criteria))
            .map(|b| b.as_ref())
    }

    /// Devices HashMap forward get
    ///
    /// * `id` - device id
    pub fn get(&self, id: &str) -> Option<&dyn Device> {
        self.devices().find(|d| d.id() == id).map(|d| d.as_ref())
    }

    /// Devices HashMap forward get_mut
    ///
    /// * `id` - device id
    pub fn get_mut(&mut self, id: &str) -> Option<&mut Box<dyn Device>> {
        self.devices.iter_mut().find(|d| d.id() == id)
    }

    /// Modifies Device in place
    ///
    /// * `q` - [`Query`] - query Device to modify
    /// * `f` - [`FnMut`] - modify function
    pub fn call<T: Device>(&mut self, q: impl Query, f: impl FnOnce(&mut T)) -> Result<(), Error> {
        self.devices
            .iter_mut()
            .find(|d| q.eq_device(*d))
            .ok_or_else(|| Error::NoSuchDevice(String::new()))
            .and_then(|d| d.as_any_mut().downcast_mut::<T>().map(f).ok_or(Error::ModifyError))
    }

    /// Modifies Device in place
    ///
    /// * `q` - [`Query`] - query Device to modify
    /// * `f` - [`FnMut`] - modify function
    pub fn call_dyn(&mut self, q: impl Query, f: impl FnOnce(&mut dyn Device)) -> Result<(), Error> {
        self.devices
            .iter_mut()
            .find(|d| q.eq_device(*d))
            .ok_or_else(|| Error::NoSuchDevice(String::new()))
            .map(|d| f(d))
    }

    pub fn query_report<Q: Query + ?Sized>(&self, criteria: &Q) -> Option<String> {
        if self.devices.is_empty() {
            None
        } else {
            let report = self
                .query(criteria)
                .fold(String::new(), |mut z, d| z + format!("\t\t{}: {d}\n", d.id()).as_str());

            if report.is_empty() {
                None
            } else {
                Some(format!("Room: {}\n", self.name) + &report)
            }
        }
    }

    /// Delete ['Device']
    pub fn remove(&mut self, name: &str) -> bool {
        let idx = self.devices().position(|d| d.id() == name);
        match idx {
            None => false,
            Some(idx) => {
                self.devices.remove(idx);
                true
            }
        }
    }

    pub fn filter<F>(&self, device_pred: F) -> impl Iterator<Item = &Box<dyn Device>>
    where
        F: Fn(&dyn Device) -> bool,
    {
        self.devices().filter(move |&d| device_pred(d))
    }

    pub fn send_cmd(&mut self, id: &str, cmd: Cmd, tx: Sender<devices::Res>) -> Result<(), Error> {
        self.get_mut(id)
            .ok_or_else(|| Error::NoSuchDevice(format!("Not found device: {}", id)))
            .map(|d| d.handle_cmd(cmd).handle(Some(tx)))
    }
}

impl Display for Room {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        if self.devices.is_empty() {
            f.write_fmt(format_args!("Room: {} \n\t\thas no device\n", self.name))
        } else {
            f.write_fmt(format_args!(
                "{}",
                self.devices().fold(format!("Room: {}\n", self.name), |z, d| {
                    z + &format!("\t\t{}: {d}\n", d.id())
                })
            ))
        }
    }
}

impl<T: Device + 'static> AddAssign<T> for Room {
    fn add_assign(&mut self, rhs: T) {
        match self.add_unique(rhs) {
            Ok(_) => (),
            Err(v) => eprintln!("{} already in room", v),
        }
    }
}

impl<D: Device + 'static> Add<D> for Room {
    type Output = Room;

    fn add(self, rhs: D) -> Self::Output {
        {
            let mut this = self;
            this.add_unique(rhs);
            this
        }
    }
}

impl SubAssign<&str> for Room {
    fn sub_assign(&mut self, rhs: &str) {
        self.remove(rhs);
    }
}

impl Sub<&str> for Room {
    type Output = Room;
    fn sub(self, rhs: &str) -> Self::Output {
        {
            let mut this = self;
            this.remove(rhs);
            this
        }
    }
}

impl Index<&str> for Room {
    type Output = dyn Device + 'static;

    fn index(&self, index: &str) -> &Self::Output {
        self.devices
            .iter()
            .find(|d| d.id() == index)
            .unwrap_or_else(|| panic!("{}", Error::NoSuchDevice(index.to_string())))
    }
}

impl<'a> IndexMut<DeviceId<'a>> for Room {
    fn index_mut(&mut self, index: RoomId<'a>) -> &mut Self::Output {
        self.get_mut(index)
            .unwrap_or_else(|| panic!("{}", Error::NoSuchDevice(index.to_string())))
    }
}
