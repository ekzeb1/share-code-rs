use std::any::Any;
use std::borrow::Borrow;
use std::fmt::{format, Debug, Display, Formatter, Pointer};
use std::ops::{Add, AddAssign, Deref, DerefMut, Index, IndexMut, Sub, SubAssign};
use std::rc::Rc;
use std::sync::mpsc::Sender;

use crate::devices::*;
use crate::home::*;
use crate::Error::{NoDevices, NoRooms, NoSuchDevice, NoSuchRoom};
use crate::{devices, Error, Query, SendError};

#[derive(Debug)]
pub struct House {
    name: String,
    rooms: Vec<Room>,
}

impl House {
    /// ['House'] with name
    pub fn new(name: &str) -> House {
        House {
            name: name.to_string(),
            rooms: Default::default(),
        }
    }

    /// [`House`] с комнатой
    pub fn with_room(mut self, room: Room) -> House {
        match self.add_unique(room) {
            Ok(_) => self,
            Err(d) => panic!("{}", Error::NotUniqueDeviceId(d.id().to_string())),
        }
    }

    pub fn add_unique(&mut self, room: Room) -> Result<(), Room> {
        if self.rooms().any(|r| r.id() == room.id()) {
            Err(room)
        } else {
            Ok(self.rooms.push(room))
        }
    }

    pub fn remove(&mut self, id: &str) -> bool {
        let idx = self.rooms().position(|r| r.id() == id);
        match idx {
            Some(idx) => {
                self.rooms.remove(idx);
                true
            }
            _ => false,
        }
    }

    /// [`Iterator`] of [`Room`] refs
    pub fn rooms(&self) -> impl Iterator<Item = &Room> {
        self.rooms.iter()
    }

    pub fn query_rooms<'a, Q: Query + ?Sized + 'a>(&'a self, q: &'a Q) -> impl Iterator<Item = &Room> + 'a {
        self.rooms().filter(move |r| q.eq_room(r))
    }

    pub fn query_devices<'a, Q: Query + ?Sized + 'a>(
        &'a self,
        q: &'a Q,
    ) -> impl Iterator<Item = &Box<dyn Device>> + 'a {
        self.rooms().filter(|r| q.eq_room(r)).flat_map(|r| r.query(q))
    }

    /// Query devices by predicates
    ///
    /// * `room_pred`   - room query Fn
    /// * `device_pred` - device query Fn
    pub fn filter<'a, RP, DP>(
        &'a self,
        room_pred: RP,
        device_pred: DP, //QUESTION: или &'a DP без Rc, но с точки зрения API как-то не очень, есть варианты еще?
    ) -> impl Iterator<Item = &Box<dyn Device>> + 'a
    where
        RP: Fn(&Room) -> bool + 'a,
        DP: Fn(&dyn Device) -> bool + 'a,
    {
        let dp = Rc::new(device_pred);

        self.rooms().filter(move |&r| room_pred(r)).flat_map(move |r| {
            let dp = Rc::clone(&dp);
            r.filter(move |d| dp(d))
        })
    }

    /// Query devices by predicates
    ///
    /// * `room_pred`   - room query fn
    /// * `device_pred` - device query fn
    pub fn filter_query<'a, Q: Query + ?Sized + 'a>(&'a self, q: &'a Q) -> impl Iterator<Item = &Box<dyn Device>> + 'a {
        self.rooms()
            .filter(move |&r| q.eq_room(r))
            .flat_map(move |r| r.filter(|d| q.eq_device(d)))
    }

    /// Query devices by id tuples
    ///
    /// * `ids` - (room, device) ids
    pub fn filter_tslice<'a>(&'a self, ids: &'a [(&str, &str)]) -> impl Iterator<Item = &Box<dyn Device>> + 'a {
        ids.iter().flat_map(|(r_id, d_id)| {
            self.rooms()
                .filter(move |r| &r.id() == r_id)
                .flat_map(move |r| r.filter(move |d| &d.id() == d_id))
        })
    }

    /// Make report by [`Query`]
    ///
    /// * `criteria` - [`Query]
    pub fn query_report<Q: Query + ?Sized>(&self, q: &Q) -> Result<String, Error> {
        if self.rooms.is_empty() {
            Err(NoRooms)
        } else {
            let mut report = self
                .query_rooms(q)
                .map(|r| r.query_report(q))
                .filter_map(|r| r.map(|report| format!("\t{}\n", report)))
                .fold(String::new(), |mut z, r| z + &r);

            if report.is_empty() {
                Err(NoSuchDevice("Device not found".to_string()))
            } else {
                Ok(format!("Smart Home: {}\n", self.name) + &report)
            }
        }
    }

    /// Call FnOnce(&mut T: Device) in place
    ///
    /// * `q` - [`Query`] - query Device to modify
    /// * `f` - [`FnOnce`] - modify function
    pub fn call<T: Device>(&mut self, q: impl Query, f: impl FnOnce(&mut T)) -> Result<(), Error> {
        self.rooms
            .iter_mut()
            .find(|r| q.eq_room(r))
            .ok_or_else(|| Error::NoSuchRoom(String::new()))
            .and_then(|r| r.call(q, f))
    }

    /// Call FnOnce(&mut dyn Device) in place
    ///
    /// * `q` - [`Query`] - query Device to modify
    /// * `f` - [`FnOnce`] - modify function
    pub fn call_dyn(&mut self, q: impl Query, f: impl FnOnce(&mut dyn Device)) -> Result<(), Error> {
        self.rooms
            .iter_mut()
            .find(|r| q.eq_room(r))
            .ok_or_else(|| Error::NoSuchRoom(String::new()))
            .and_then(|r| r.call_dyn(q, f))
    }

    ///  forward get
    ///
    /// * `id` - device id
    pub fn get(&self, id: &str) -> Option<&Room> {
        self.rooms().find(|d| d.id() == id)
    }

    /// Devices HashMap forward get_mut
    ///
    /// * `id` - device id
    pub fn get_mut(&mut self, id: &str) -> Option<&mut Room> {
        self.rooms.iter_mut().find(|d| d.id() == id)
    }

    /// Query devices by ids
    ///
    /// * `ids` - [r_id, d_id, r_id, d_id, ...]
    pub fn filter_slice<'a>(&'a self, ids: &'a [&str]) -> impl Iterator<Item = &Box<dyn Device>> + 'a {
        ids.chunks(2)
            .into_iter()
            .filter_map(|ids| match ids {
                &[r_id, d_id] => Some(
                    self.rooms()
                        .filter(move |r| r.id() == r_id)
                        .flat_map(move |r| r.filter(move |d| d.id() == d_id)),
                ),
                _ => None,
            })
            .flatten()
    }

    /// Query devices by predicates
    ///
    /// * `room_pred`   - room query fn
    /// * `device_pred` - device query fn
    pub fn filter_fn(
        &self,
        room_pred: fn(&Room) -> bool,
        device_pred: fn(&dyn Device) -> bool,
    ) -> impl Iterator<Item = &Box<dyn Device>> {
        self.rooms()
            .filter(move |&r| room_pred(r))
            .flat_map(move |r| r.filter(device_pred))
    }

    pub fn send_cmd(&mut self, id: Id, cmd: Cmd) -> Result<Res, Error> {
        self.get_mut(id.room_id())
            .ok_or_else(|| NoSuchRoom(id.room_id().to_string()))
            .and_then(|room| {
                let (tx, rx) = std::sync::mpsc::channel::<Res>();
                room.send_cmd(id.device_id(), cmd, tx)
                    .and_then(|_| rx.recv().map_err(|e| e.into()))
            })
    }
}

impl Display for House {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!("Smart Home: {}\n\n", self.name));
        let rooms = self.rooms().fold(String::new(), |z, r| z + &format!("\t{r}\n"));
        f.write_fmt(format_args!("{}", rooms))
    }
}

impl std::ops::Add<Room> for House {
    type Output = House;

    fn add(self, rhs: Room) -> House {
        {
            let mut this = self;
            match this.add_unique(rhs) {
                Ok(e) => (),
                Err(d) => eprintln!("{}", Error::NotUniqueDeviceId(d.id().to_string())),
            }
            this
        }
    }
}

impl AddAssign<Room> for House {
    fn add_assign(&mut self, rhs: Room) {
        self.add_unique(rhs);
    }
}

impl SubAssign<&str> for House {
    fn sub_assign(&mut self, rhs: &str) {
        self.remove(rhs);
    }
}

impl std::ops::Sub<&str> for House {
    type Output = House;

    fn sub(self, rhs: &str) -> Self::Output {
        {
            let mut this = self;
            this.remove(rhs);
            this
        }
    }
}

impl<'a, D: Device + 'static> AddAssign<(RoomId<'a>, D)> for House {
    fn add_assign(&mut self, rhs: (&str, D)) {
        match self.get_mut(rhs.0).map(|r| r.add_unique(rhs.1)) {
            Some(Ok(_)) => {}
            Some(Err(d)) => eprintln!("{}", Error::NotUniqueDeviceId(d.id().to_string())),
            _ => eprintln!("{}", Error::NoSuchRoom(rhs.0.to_string())),
        }
    }
}

impl<'a> SubAssign<(RoomId<'a>, DeviceId<'a>)> for House {
    fn sub_assign(&mut self, rhs: (RoomId<'a>, DeviceId<'a>)) {
        match self.get_mut(rhs.0).map(|r| *r -= rhs.1) {
            Some(_) => {}
            _ => eprintln!("{}", Error::NoSuchRoom(rhs.0.to_string())),
        }
    }
}

impl<'a> Index<RoomId<'a>> for House {
    type Output = Room;

    fn index(&self, index: RoomId<'a>) -> &Self::Output {
        self.rooms
            .iter()
            .find(|r| r.id() == index)
            .unwrap_or_else(|| panic!("{}", Error::NoSuchRoom(index.to_string())))
    }
}

impl<'a> IndexMut<RoomId<'a>> for House {
    fn index_mut(&mut self, index: RoomId<'a>) -> &mut Self::Output {
        self.get_mut(index)
            .unwrap_or_else(|| panic!("{}", Error::NoSuchRoom(index.to_string())))
    }
}

impl<'a> Index<(RoomId<'a>, DeviceId<'a>)> for House {
    type Output = dyn Device + 'static;

    fn index(&self, index: (RoomId<'a>, DeviceId<'a>)) -> &Self::Output {
        self.rooms()
            .find(|r| r.id() == index.0)
            .and_then(|r| r.devices.iter().find(|d| d.id() == index.1))
            .unwrap_or_else(|| panic!("{}", Error::NoSuchDevice(format!("{},{}", index.0, index.1))))
    }
}

impl<'a> IndexMut<(RoomId<'a>, DeviceId<'a>)> for House {
    fn index_mut(&mut self, index: (RoomId<'a>, DeviceId<'a>)) -> &mut Self::Output {
        self.get_mut(index.0)
            .and_then(|r| r.get_mut(index.1))
            .unwrap_or_else(|| panic!("{}", Error::NoSuchDevice(format!("{},{}", index.0, index.1))))
    }
}

impl Default for House {
    fn default() -> Self {
        House::new("House")
            .with_room(
                Room::new("Kitchen")
                    .with_device(Socket::new("Socket"))
                    .with_device(Thermometer::new("Thermometer")),
            )
            .with_room(
                Room::new("Bed Room")
                    .with_device(Thermometer::new("Thermometer"))
                    .with_device(Socket::new("Socket")),
            )
    }
}
