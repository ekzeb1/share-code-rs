use crate::devices::Device;
use crate::home::*;

use serde::{Deserialize, Serialize};
use std::fmt::{Debug, Display, Formatter};

#[derive(Serialize, Deserialize, Default, Copy, Clone)]
pub enum EntityQuery {
    #[default]
    RoomQ,
    DeviceQ,
}

pub trait Query {
    fn eq_room(&self, room: &Room) -> bool;
    fn eq_device(&self, device: &dyn Device) -> bool;
}

impl Query for dyn Device + '_ {
    fn eq_room(&self, room: &Room) -> bool {
        true
    }
    fn eq_device(&self, device: &dyn Device) -> bool {
        self.tpe() == device.tpe()
    }
}

impl Query for Room {
    fn eq_room(&self, room: &Room) -> bool {
        self.id() == room.id()
    }
    fn eq_device(&self, device: &dyn Device) -> bool {
        true
    }
}

#[derive(Default)]
pub struct QueryFn(Option<fn(&Room) -> bool>, Option<fn(&dyn Device) -> bool>);

impl QueryFn {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn from(room_pred: fn(&Room) -> bool, device_pred: fn(&dyn Device) -> bool) -> Self {
        QueryFn(Some(room_pred), Some(device_pred))
    }

    pub fn from_device_fn(device_pred: fn(&dyn Device) -> bool) -> Self {
        QueryFn(None, Some(device_pred))
    }

    pub fn from_room_fn(room_pred: fn(&Room) -> bool) -> Self {
        QueryFn(Some(room_pred), None)
    }

    pub fn with_device_fn(mut self, pred: fn(&dyn Device) -> bool) -> Self {
        self.1 = Some(pred);
        self
    }

    pub fn with_room_fn(mut self, pred: fn(&Room) -> bool) -> Self {
        self.0 = Some(pred);
        self
    }
}

impl Query for QueryFn {
    fn eq_room(&self, room: &Room) -> bool {
        match self.0 {
            Some(pred) => pred(room),
            _ => true,
        }
    }

    fn eq_device(&self, device: &dyn Device) -> bool {
        match self.1 {
            Some(pred) => pred(device),
            _ => true,
        }
    }
}
/// By room & device ids query
///
/// * `0` - room_id
/// * `1` = device_id
impl Query for (&str, &str) {
    fn eq_room(&self, room: &Room) -> bool {
        self.0 == room.id()
    }
    fn eq_device(&self, device: &dyn Device) -> bool {
        self.1 == device.id()
    }
}

/// By device id query
///
/// * `0` - device_id. Not stable & will return first eq device in any Room from HashMap
impl Query for (&str,) {
    fn eq_room(&self, room: &Room) -> bool {
        true
    }
    fn eq_device(&self, device: &dyn Device) -> bool {
        self.0 == device.id()
    }
}

/// By room & device ids query
///
/// * `0` - room_id
/// * `1` = device_id
impl Query for (String, String) {
    fn eq_room(&self, room: &Room) -> bool {
        self.0 == room.id()
    }
    fn eq_device(&self, device: &dyn Device) -> bool {
        self.1.as_str() == device.id()
    }
}

/// By device id query
///
/// * `0` - device_id. Not stable & will return first eq device in any Room from HashMap
impl Query for (String,) {
    fn eq_room(&self, room: &Room) -> bool {
        true
    }
    fn eq_device(&self, device: &dyn Device) -> bool {
        self.0.as_str() == device.id()
    }
}

impl Query for (&str, EntityQuery) {
    fn eq_room(&self, room: &Room) -> bool {
        if matches!(self.1, EntityQuery::RoomQ) {
            return self.0 == room.id();
        }
        true
    }

    fn eq_device(&self, device: &dyn Device) -> bool {
        if matches!(self.1, EntityQuery::DeviceQ) {
            return self.0 == device.id();
        }
        true
    }
}

impl Query for (String, EntityQuery) {
    fn eq_room(&self, room: &Room) -> bool {
        Query::eq_room(&(self.0.as_str(), self.1), room)
    }

    fn eq_device(&self, device: &dyn Device) -> bool {
        Query::eq_device(&(self.0.as_str(), self.1), device)
    }
}
