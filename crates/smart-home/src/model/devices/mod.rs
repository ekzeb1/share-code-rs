use byteorder::ReadBytesExt;
use serde::{Deserialize, Serialize};
use std::any::{Any, TypeId};
use std::fmt::{format, Debug, Display, Formatter, Pointer, Write};
use std::io::Read;
use std::str::FromStr;
use std::sync::mpsc::Sender;
use std::sync::Arc;

//pub mod msgs;
mod socket;
mod thermometer;

use crate::{Error, ParseError, Query, Room};
pub use socket::*;
pub use thermometer::*;

pub trait Identity {
    fn id(&self) -> &str;
    fn tpe(&self) -> TypeId;
    fn as_any(&self) -> &dyn Any;
    fn as_any_mut(&mut self) -> &mut dyn Any;
}

pub trait Device: Display + Debug + Send + Sync + 'static {
    fn on(&mut self) {}
    fn off(&mut self) {}

    fn id(&self) -> &str;
    fn tpe(&self) -> TypeId;
    fn as_any(&self) -> &dyn Any;
    fn as_any_mut(&mut self) -> &mut dyn Any;
    fn handle_cmd(&mut self, cmd: Cmd) -> Res;
}

impl PartialEq for dyn Device + '_ {
    fn eq(&self, other: &Self) -> bool {
        self.tpe() == other.tpe()
    }
}

impl Eq for dyn Device + '_ {}

impl Device for Box<dyn Device> {
    fn on(&mut self) {
        self.as_mut().on()
    }

    fn off(&mut self) {
        self.as_mut().off()
    }

    fn id(&self) -> &str {
        self.as_ref().id()
    }

    fn tpe(&self) -> TypeId {
        self.as_ref().tpe()
    }

    fn as_any(&self) -> &dyn Any {
        self.as_ref().as_any()
    }

    fn as_any_mut(&mut self) -> &mut dyn Any {
        self.as_mut().as_any_mut()
    }

    fn handle_cmd(&mut self, cmd: Cmd) -> Res {
        self.as_mut().handle_cmd(cmd)
    }
}

#[derive(Debug, Default, PartialEq, Eq, Deserialize, Serialize, Clone)]
pub struct Id(pub String, pub String);

impl Id {
    pub fn room_id(&self) -> &str {
        self.0.as_str()
    }

    pub fn device_id(&self) -> &str {
        self.1.as_str()
    }

    pub fn new(room_id: &str, device_id: &str) -> Id {
        Id(room_id.to_string(), device_id.to_string())
    }
}

impl From<(&str, &str)> for Id {
    fn from(ids: (&str, &str)) -> Self {
        Id(ids.0.to_string(), ids.1.to_string())
    }
}

impl Query for Id {
    fn eq_room(&self, room: &Room) -> bool {
        self.0 == room.id()
    }

    fn eq_device(&self, device: &dyn Device) -> bool {
        self.1 == device.id()
    }
}

impl FromStr for Id {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s
            .trim()
            .split('~')
            .filter_map(|s| match s.trim() {
                "" => None,
                t => Some(t),
            })
            .collect::<Vec<_>>()
            .as_slice()
        {
            [room, device] => Ok(Id(room.to_string(), device.to_string())),
            _ => Err(ParseError(format!("invalid Id format {}", s))),
        }
    }
}

impl Display for Id {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!("{}~{}", self.0, self.1))
    }
}

#[derive(Debug, Default, PartialEq, Eq, Deserialize, Serialize, Clone)]
pub enum Cmd {
    #[default]
    Noop,
    TurnOn,
    TurnOff,
    Gen(u8, Vec<u8>),
}

use Cmd::*;

impl Cmd {
    const NOOP: u8 = b'0';
    const TURN_ON: u8 = b'1';
    const TURN_OFF: u8 = b'2';

    pub fn id(&self) -> u8 {
        match self {
            Gen(n, _) => *n,
            TurnOn => Self::TURN_ON,
            TurnOff => Self::TURN_OFF,
            Noop => Self::NOOP,
        }
    }
}

impl From<Cmd> for Vec<u8> {
    fn from(c: Cmd) -> Self {
        match c {
            Gen(n, bs) if n > Cmd::TURN_OFF => {
                let mut result = vec![n];
                result.extend_from_slice(&bs);
                result
            }
            Gen(n, _) => panic!("Invalid Gen msg format. First byte <= 50: {}", n),
            Noop => vec![Cmd::NOOP],
            TurnOn => vec![Cmd::TURN_ON],
            TurnOff => vec![Cmd::TURN_OFF],
        }
    }
}

impl TryFrom<&[u8]> for Cmd {
    type Error = Error;

    fn try_from(value: &[u8]) -> Result<Self, Self::Error> {
        match value {
            [Cmd::TURN_ON] => Ok(TurnOn),
            [Cmd::TURN_OFF] => Ok(TurnOff),
            [Cmd::NOOP] => Ok(Noop),
            [n, bs @ ..] if n > &Cmd::TURN_OFF => Ok(Gen(*n, bs.to_vec())),
            _ => Err(Error::ParseError(format!(
                "Invalid Gen msg format. First byte value <= 50 : {:?}",
                value
            ))),
        }
    }
}

impl FromStr for Cmd {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut bytes = s.as_bytes();
        if bytes.len() == 1 {
            bytes.try_into()
        } else {
            let mut result = vec![bytes.read_u8()?];
            let len = hex::decode(&bytes[0..2])?.as_slice().read_u8()?;
            if len > 0 {
                let value = hex::decode(&s[3..(3 + (len as usize))])
                    .map_err(|e| ParseError(format!("len != value hex len '{}'", e)))?;
                result.extend_from_slice(&value);
            }
            result.as_slice().try_into()
        }
    }
}

/// first byte Mgs id
impl Display for Cmd {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Noop => f.write_str("0"),
            TurnOn => f.write_str("1"),
            TurnOff => f.write_str("2"),
            Gen(n, bs) => {
                if bs.is_empty() {
                    f.write_fmt(format_args!("{}00", std::str::from_utf8(&[*n; 1]).unwrap()))
                } else {
                    let value_hex = hex::encode(bs);
                    let len_hex = hex::encode((value_hex.len() as u8).to_be_bytes());
                    f.write_fmt(format_args!(
                        "{}{}{}",
                        std::str::from_utf8(&[*n; 1]).unwrap(),
                        len_hex,
                        value_hex
                    ))
                }
            }
        }
    }
}

#[derive(Deserialize, Serialize, Debug, Clone)]
pub enum Res {
    Ok(u8, Option<Vec<u8>>),
    Err(String),
}

impl Res {
    pub fn ok(cmd: &Cmd, v: &[u8]) -> Res {
        Res::Ok(cmd.id(), if v.is_empty() { None } else { Some(v.to_vec()) })
    }

    pub fn err(s: &str) -> Res {
        Res::Err(s.to_string())
    }
}

/// Res.0 = msg_id, Res.1 big_endian res_value
/// {u8:msg_id}{u16:hex_res_value_be_length}{hex:res_value}
impl Display for Res {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Res::Ok(id, None) => f.write_fmt(format_args!("{}", std::str::from_utf8(&[*id; 1]).unwrap())),
            Res::Ok(id, Some(bs)) => {
                let value_hex = hex::encode(bs);
                if value_hex.len() > u8::MAX as usize {
                    f.write_fmt(format_args!(
                        "Gen value length in hex:{} exceeds max u8 size!",
                        value_hex.len()
                    ));
                    return Err(std::fmt::Error);
                }
                let len_hex = hex::encode((value_hex.len() as u8).to_be_bytes());
                f.write_fmt(format_args!(
                    "{}{}{}",
                    std::str::from_utf8(&[*id; 1]).unwrap(),
                    len_hex,
                    value_hex
                ))
            }
            Res::Err(err) => f.write_fmt(format_args!("{}", err)),
        }
    }
}

/// [`Cmd`] -> [`Res`] handling

pub trait CmdResult {
    fn handle(self, tx: Option<Sender<Res>>);
}

impl CmdResult for Res {
    fn handle(self, tx: Option<Sender<Res>>) {
        if let Some(tx) = tx {
            let _ = tx.send(self);
        }
    }
}

pub trait CmdHandler {
    type Response: CmdResult;
    fn handle(&mut self, msg: Cmd) -> Self::Response;
}

#[cfg(test)]
mod tests {
    use super::*;
    use byteorder::BigEndian;

    #[test]
    fn test_hex() {
        println!("{}", hex::encode(vec![52]))
    }

    #[test]
    fn test_cmd_serde() {
        println!("{}", Cmd::Gen(52u8, vec![52]));
        assert_eq!(vec![48u8], <Vec<u8>>::from(Cmd::Noop));
        assert_eq!(vec![49u8], <Vec<u8>>::from(Cmd::TurnOn));
        assert_eq!(vec![50u8], <Vec<u8>>::from(Cmd::TurnOff));
        assert_eq!(vec![51u8, 52], <Vec<u8>>::from(Cmd::Gen(51, vec![52])));
        assert_eq!("2", Cmd::TurnOff.to_string());
        assert_eq!("40234", Cmd::Gen(52, vec![52]).to_string());
        assert_eq!(Cmd::TurnOff, "2".parse::<Cmd>().unwrap());
        assert_eq!(Cmd::Gen(52, vec![52]), "40234".parse::<Cmd>().unwrap());
    }
}
