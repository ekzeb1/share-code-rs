use crate::devices::Cmd::Noop;
use crate::devices::CmdHandler;
use crate::devices::{Cmd, Res};
use crate::SendError;
use byteorder::{BigEndian, ReadBytesExt};
use std::any::{Any, TypeId};
use std::fmt::{Debug, Display, Formatter, Pointer, Write};
use std::io::Cursor;
use std::sync::mpsc::Sender;

use super::Device;

/// Electric socket twin object
///
/// * `power` - electric power in Watts
/// * `state` - on/off state
/// * `id`    - unique in room socket id, used as key in [`Room`] devices map
#[derive(Clone, Debug, Default, Eq, PartialEq)]
pub struct Socket {
    power: u16,
    state: SocketState,
    id: String,
}

/// Constructor func for [`Socket`]
///
/// * `power` - electric power in Watts
/// * `state` - on/off state
/// * `id`    - unique in room socket id, used as key in [`Room`] devices map
pub fn Socket(power: u16, state: SocketState, id: &str) -> Socket {
    Socket {
        power,
        state,
        id: id.to_string(),
    }
}

impl Socket {
    /// Cmd u8 ids
    ///
    /// * `GET` - get [`Gen`] cmd
    pub const GET: u8 = b'3';
    /// * `SET` - set [`Gen`] cmd
    pub const SET: u8 = b'4';

    /// Constructor func for [`Socket`]
    ///
    /// * `id` - unique in room socket id, used as key in [`Room`] devices map
    pub fn new(id: &str) -> Socket {
        Socket {
            id: id.to_string(),
            ..Self::default()
        }
    }

    pub fn current_power_consumption(&self) -> u16 {
        self.power
    }

    pub fn set_power(&mut self, power: u16) {
        self.power = power
    }

    pub fn current_state(&self) -> SocketState {
        self.state
    }
}

impl Display for Socket {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self.state {
            SocketState::Off => f.write_str("state off"),
            SocketState::On => f.write_fmt(format_args!(
                "state on, consumed power {}W ",
                self.current_power_consumption()
            )),
        }
    }
}

impl Device for Socket {
    fn on(&mut self) {
        self.state = SocketState::On
    }

    fn off(&mut self) {
        self.state = SocketState::Off
    }

    fn id(&self) -> &str {
        self.id.as_str()
    }

    fn tpe(&self) -> TypeId {
        self.type_id()
    }

    fn as_any(&self) -> &dyn Any {
        self
    }

    fn as_any_mut(&mut self) -> &mut dyn Any {
        self
    }

    fn handle_cmd(&mut self, cmd: Cmd) -> Res {
        self.handle(cmd)
    }
}

/// Electric socket state
#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum SocketState {
    Off,
    On,
}

impl Default for SocketState {
    fn default() -> Self {
        SocketState::Off
    }
}

impl Display for SocketState {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        Debug::fmt(self, f)
    }
}

impl CmdHandler for Socket {
    type Response = Res;

    fn handle(&mut self, cmd: Cmd) -> Self::Response {
        const GET: u8 = Socket::GET;
        const SET: u8 = Socket::SET;

        match cmd {
            // 1
            ref e @ Cmd::TurnOn => {
                self.on();
                Res::ok(e, &[])
            }
            // 2
            ref e @ Cmd::TurnOff => {
                self.off();
                Res::ok(e, &[])
            }
            // 3
            ref e @ Cmd::Gen(GET, _) => {
                let t = self.power.to_be_bytes();
                Res::ok(e, &t)
            }
            // 4u16
            ref e @ Cmd::Gen(SET, ref val) if val.len() == 2 => match val.as_slice().read_u16::<BigEndian>() {
                Err(e) => Res::Err(format!("Socket['{}'] error read u16 from {val:?}: {e}", self.id())),
                Ok(n) => {
                    self.set_power(n);
                    println!("Socket['{}']: new power {n}W", self.id());
                    Res::ok(e, &self.power.to_be_bytes())
                }
            },

            ref e @ Cmd::Gen(SET, ref val) => Res::Err(format!("Socket['{}'] error not u16 {val:?} in {e}", self.id())),

            ufo => Res::Err(format!("Socket['{}'] unknown cmd {ufo}", self.id())),
        }
    }
}
