use crate::devices::{Cmd, Device, Res};
use std::sync::mpsc::Sender;
use std::sync::Arc;

// handlers quasi actix =)

pub trait Msg<D: Device + ?Sized> {
    type Response: 'static;
}

impl<D: Device + ?Sized, M: Msg<D>> Msg<D> for Arc<M> {
    type Response = M::Response;
}

impl<D: Device + ?Sized, M: Msg<D>> Msg<D> for Box<M> {
    type Response = M::Response;
}

pub trait MsgResponse<D: Device + ?Sized, M: Msg<D>> {
    fn handle(self, tx: Option<Sender<M::Response>>);
}

pub trait MsgHandler<M: Msg<Self>>
where
    Self: Device,
{
    type Response: MsgResponse<Self, M>;

    fn handle(&mut self, msg: M) -> Self::Response;
}

macro_rules! msg_response {
    ($($selft:ty),*) => {
        $(
            impl<D, M> MsgResponse<D, M> for $selft
            where
                D: Device + SendHandler<M>,
                M: Msg<D, Response = $selft>,
            {
                fn handle(self, tx: Option<Sender<M::Response>>) {
                    if let Some(tx) = tx {
                        let _ = tx.send(self);
                    }
                }
            }
        )*
    }
}

msg_response!(u8, u16, u32, u64, u128, usize);
msg_response!(i8, i16, i32, i64, i128, isize);
msg_response!((), bool, f32, f64, String);
