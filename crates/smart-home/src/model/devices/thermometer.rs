use crate::Error;
use byteorder::{BigEndian, ReadBytesExt};
use smart_home_macro::Device;
use std::any::{Any, TypeId};
use std::fmt::{Debug, Display, Formatter, Pointer, Write};
use std::io::Cursor;
use std::sync::mpsc::Sender;

use super::*;

/// Thermometer device twin object
///
/// * `temperature` - current temperature
/// * `id`          - unique in room thermometer id, used as key in [`Room`] devices map
#[derive(Clone, Debug, Default, Eq, PartialEq, Device)]
pub struct Thermometer {
    temperature: i16,
    #[id_field]
    id: String,
}

/// Constructor func for ['Thermometer']
pub fn Thermometer(temperature: i16, name: &str) -> Thermometer {
    Thermometer {
        temperature,
        id: name.to_string(),
    }
}

impl Thermometer {
    /// Cmd u8 ids
    ///
    /// * `GET` - get [`Gen`] cmd
    pub const GET: u8 = b'3';
    /// * `SET` - set [`Gen`] cmd
    pub const SET: u8 = b'4';

    pub fn new(id: &str) -> Thermometer {
        Self {
            id: id.to_string(),
            ..Self::default()
        }
    }

    pub fn temperature(&self) -> i16 {
        self.temperature
    }

    pub fn set_temperature(&mut self, v: i16) {
        self.temperature = v
    }
}

impl Display for Thermometer {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!("temperature {}C°", self.temperature))
    }
}

impl CmdHandler for Thermometer {
    type Response = Res;

    fn handle(&mut self, msg: Cmd) -> Self::Response {
        const GET: u8 = Thermometer::GET;
        const SET: u8 = Thermometer::SET;

        match msg {
            ref e @ Cmd::TurnOn => {
                self.on();
                Res::ok(e, &[])
            }
            ref e @ Cmd::TurnOff => {
                self.off();
                Res::ok(e, &[])
            }
            ref e @ Cmd::Gen(GET, _) => {
                let t = self.temperature.to_be_bytes();
                Res::ok(e, &t)
            }

            ref e @ Cmd::Gen(SET, ref cmd) if cmd.len() == 2 => match cmd.as_slice().read_i16::<BigEndian>() {
                Err(e) => Res::Err(format!("Thermometer error read i16 from {cmd:?}: {e}")),
                Ok(n) => {
                    self.set_temperature(n);
                    println!("Thermometer[{}]: new temperature {n}C°", self.id());
                    Res::ok(e, &self.temperature.to_be_bytes())
                }
            },

            ref e @ Cmd::Gen(SET, ref cmd) => Res::Err(format!("Thermometer error no i16 {cmd:?} in {e}")),

            msg => Res::Err(format!("Thermometer unknown msg {msg}")),
        }
    }
}
