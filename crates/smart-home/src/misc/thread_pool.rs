use std::fmt::{Display, Formatter};
use std::num::NonZeroUsize;
use std::sync::mpsc;
use std::sync::{Arc, Mutex};
use std::thread;

type Job = Box<dyn FnOnce() + Send + 'static>;

enum Task {
    New(Job),
    Exit,
}

#[derive(Debug)]
struct Worker {
    id: usize,
    handle: Option<thread::JoinHandle<()>>,
}

impl Display for Worker {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!("Worker(id: {}, ..)", self.id))
    }
}

impl Worker {
    fn new(id: usize, receiver: Arc<Mutex<mpsc::Receiver<Task>>>) -> Self {
        let handle = thread::spawn(move || loop {
            let task = {
                let rx = receiver.lock().unwrap();
                rx.recv()
                    .unwrap_or_else(|e| panic!("Error {} recv on worker[{}] receiver", e, id))
            };

            match task {
                Task::New(job) => job(),
                Task::Exit => break,
            }
        });

        Worker {
            id,
            handle: Some(handle),
        }
    }
}

pub struct ThreadPool {
    workers: Vec<Worker>,
    sender: mpsc::Sender<Task>,
}

impl ThreadPool {
    pub fn with_capacity(size: usize) -> Self {
        if size == 0 {
            panic!("Invalid thread_pool capacity == 0");
        }
        let mut workers = Vec::with_capacity(size);

        let (sender, recv) = mpsc::channel();
        let recv = Arc::new(Mutex::new(recv));

        for id in 0..size {
            workers.push(Worker::new(id, Arc::clone(&recv)));
        }

        ThreadPool { workers, sender }
    }

    pub fn new() -> Self {
        let par = thread::available_parallelism().map(|v| v.get()).unwrap_or_else(|_| 4);

        ThreadPool::with_capacity(par)
    }

    pub fn run<J>(&self, job: J)
    where
        J: FnOnce() + Send + 'static,
    {
        let job = Box::new(job);
        let _ = self.sender.send(Task::New(job));
    }
}

impl Drop for ThreadPool {
    fn drop(&mut self) {
        for _ in &self.workers {
            let _ = self.sender.send(Task::Exit);
        }
        for worker in self.workers.iter_mut() {
            if let Some(handle) = worker.handle.take() {
                let _ = handle.join();
            }
        }
    }
}

impl Default for ThreadPool {
    fn default() -> Self {
        ThreadPool::new()
    }
}
