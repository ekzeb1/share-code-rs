//! Smart Home
//! ==========
//!
//!

#![allow(non_snake_case)]
#![allow(unused)]
#![allow(clippy::unit_arg)]

extern crate core;

pub(crate) mod misc;
pub(crate) mod model;
pub(crate) mod network;

pub use model::devices;
pub use model::home::*;
pub use model::query::*;
pub use model::*;

pub use network::async_tcp;
pub use network::tcp;
pub use network::udp;
use serde::{Deserialize, Serialize};

use crate::devices::Res;
use crate::Error::*;
use bincode::Error as BinError;
use hex::FromHexError;
use std::fmt::{Display, Formatter, Write};
use std::str::Utf8Error;
use std::string::FromUtf8Error;
use std::sync::mpsc::RecvError;

#[non_exhaustive]
#[derive(Debug)]
pub enum Error {
    NoSuchDevice(String),
    NoSuchRoom(String),
    NoRooms,
    NoDevices,
    ModifyError,
    NotUniqueDeviceId(String),
    UnknownReq(String),
    ServerError(std::io::Error),
    ParseError(String),
    SendError(String),
    ClientError(String),
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Error::NoSuchDevice(id) => f.write_fmt(format_args!("No such device: {}", id)),
            Error::NoSuchRoom(name) => f.write_fmt(format_args!("No such room: {}", name)),
            Error::NoRooms => f.write_str("No rooms in Home"),
            Error::NoDevices => f.write_str("No devices in Room"),
            Error::ModifyError => f.write_str("Modify device error"),
            Error::NotUniqueDeviceId(id) => f.write_fmt(format_args!("Device with {} id already exists", id)),
            Error::UnknownReq(e) => f.write_fmt(format_args!("Unknown req: {}", e)),
            Error::ServerError(e) => f.write_fmt(format_args!("Tcp server err: {}", e)),
            Error::ParseError(e) => f.write_fmt(format_args!("Network request parse error: {}", e)),
            Error::SendError(s) => f.write_fmt(format_args!("Send msg to device error: {}", s)),
            Error::ClientError(s) => f.write_fmt(format_args!("str_client error {}", s)),
        }
    }
}

impl std::error::Error for Error {}

impl From<std::io::Error> for Error {
    fn from(e: std::io::Error) -> Self {
        ServerError(e)
    }
}

impl From<Utf8Error> for Error {
    fn from(e: Utf8Error) -> Self {
        ParseError(e.to_string())
    }
}

impl From<FromUtf8Error> for Error {
    fn from(e: FromUtf8Error) -> Self {
        ParseError(e.to_string())
    }
}

impl From<Error> for std::io::Error {
    fn from(e: Error) -> Self {
        match e {
            ServerError(e) => e,
            e => panic!("unknown err to io {}", e),
        }
    }
}

impl From<std::sync::mpsc::SendError<devices::Res>> for Error {
    fn from(e: std::sync::mpsc::SendError<Res>) -> Self {
        SendError(e.to_string())
    }
}

impl From<std::sync::mpsc::RecvError> for Error {
    fn from(e: RecvError) -> Self {
        SendError(e.to_string())
    }
}

impl From<BinError> for Error {
    fn from(e: BinError) -> Self {
        ParseError(e.to_string())
    }
}

impl From<FromHexError> for Error {
    fn from(e: FromHexError) -> Self {
        ParseError(e.to_string())
    }
}

impl Error {
    pub fn no_such_device(msg: &str) -> Error {
        NoSuchDevice(msg.to_string())
    }
}
