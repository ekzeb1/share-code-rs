#![allow(non_snake_case)]

use serde::{Deserialize, Serialize};
use std::io::prelude::*;

use encoding_rs::WINDOWS_1251;
use quick_xml::se::to_string;

use std::fs::File;

use log::info;
use log::warn;
use quick_xmlx as ext;
use quick_xmlx::{DeserializeXmlExt, SerializeXmlExt};
use simple_logger::SimpleLogger;

#[derive(Deserialize, Serialize, Debug)]
#[serde(rename = "Файл")]
pub struct DocFile {
    #[serde(rename = "ИдФайл", default)]
    idFile: String,
    #[serde(rename = "ВерсФорм", default)]
    versForm: String,
    #[serde(rename = "ВерсПрог", default)]
    versProg: String,
    #[serde(rename = "СвУчДокОбор")]
    svUchDocObor: DocFlowPartsInfo,
    #[serde(rename = "Документ")]
    doc: Doc,
}

#[derive(Deserialize, Serialize, Debug)]
#[serde(rename = "Документ")]
struct Doc {
    //#[serde(rename = "КНД", default)]
    КНД: String,
    //#[serde(rename = "Функция", default)]
    Функция: String,
    //#[serde(rename = "ПоФактХЖ", default)]
    ПоФактХЖ: String,
    //#[serde(rename = "НаимДокОпр", default)]
    НаимДокОпр: String,
    //#[serde(rename = "ДатаИнфПр", default)]
    ДатаИнфПр: String,
    //#[serde(rename = "ВремИнфПр", default)]
    ВремИнфПр: String,
    //#[serde(rename = "НаимЭконСубСост", default)]
    НаимЭконСубСост: String,
    //#[serde(rename = "Подписант")]
    Подписант: Подписант,
}

#[derive(Deserialize, Serialize, Debug)]
//#[serde(rename = "Подписант")]
struct Подписант {
    //#[serde(rename = "ОблПолн", default)]
    ОблПолн: i32,
    //#[serde(rename = "Статус", default)]
    Статус: i32,
    //#[serde(rename = "ОснПолн", default)]
    ОснПолн: String,
    //#[serde(rename = "ОснПолнОрг", default)]
    ОснПолнОрг: i32,
}

#[derive(Deserialize, Serialize, Debug)]
#[serde(rename = "СвУчДокОбор")]
struct DocFlowPartsInfo {
    //#[serde(rename = "ИдОтпр", default)]
    ИдОтпр: String,
    //#[serde(rename = "ИдПол", default)]
    ИдПол: String,
    //#[serde(rename = "СвОЭДОтпр")]
    СвОЭДОтпр: СвОЭДОтпр,
}

#[derive(Deserialize, Serialize, Debug)]
//#[serde(rename = "СвОЭДОтпр")]
struct СвОЭДОтпр {
    //#[serde(rename = "НаимОрг", default)]
    НаимОрг: String,
    //#[serde(rename = "ИННЮЛ", default)]
    ИННЮЛ: String,
    //#[serde(rename = "ИдЭДО", default)]
    ИдЭДО: String,
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    SimpleLogger::new().init().unwrap();

    let n: i32 = 12;

    let upd = DocFile::from_reader_decoded(&mut File::open("crates/quick-xmlx/upd.xml")?)?;

    info!("\nUPD\n{:#?}\n\n", upd);

    let xml = to_string(&upd)?;

    let xml = format!("{}{}", ext::XmlDecl::Ver1(WINDOWS_1251), xml);

    let win = WINDOWS_1251.encode(&xml);

    let mut fff = File::create("crates/quick-xmlx/new_upd.xml")?;

    let (enc, ..) = upd.encode_to_writer(&mut fff, WINDOWS_1251, false)?;

    warn!("ENCODING \n\n{:?}\n\n", enc);

    let new_old_upd =
        DocFile::decode_from_reader(File::open("crates/quick-xmlx/new_upd.xml")?, WINDOWS_1251)?;

    warn!("NEW_OLD_XML:\n{:?}", new_old_upd);

    let upd_from_str = DocFile::from_str(&xml)?;

    warn!("from_str:\n{:?}\n", upd_from_str);

    let mut new_file = File::create("crates/quick-xmlx/new_upd.xml")?;

    new_file.write_all(&win.0)?;

    let new_xml =
        DocFile::decode_from_reader(File::open("crates/quick-xmlx/new_upd.xml")?, WINDOWS_1251)?;

    info!("NEW_FROM_XML_FILE:\n{:#?}\n", new_xml);

    let xml_str =
        ext::decode_reader_to_string(File::open("crates/quick-xmlx/new_upd.xml")?, WINDOWS_1251)?;

    info!("XML_STR:\n{}\n", xml_str);

    let xml_file: DocFile = quick_xml::de::from_str(&xml_str)?;

    info!("FILE:\n{:#?}", xml_file);

    //std::fs::remove_file("quick-xmlx/new_upd.xml")?;

    Ok(())
}
