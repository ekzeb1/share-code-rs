#![allow(non_snake_case)]

use std::borrow::Cow::{Borrowed, Owned};
use std::fmt::{Debug, Display, Formatter};
use std::io::prelude::*;
use std::io::{BufReader, ErrorKind};

use crate::XmlDecl::*;
use encoding_rs::{Encoding, UTF_8};
use encoding_rs_io::DecodeReaderBytesBuilder;
use quick_xml::de;
use quick_xml::{se, DeError};
use serde::de::DeserializeOwned;
use serde::Serialize;
use chardetng::EncodingDetector;

#[derive(Debug)]
pub enum XmlDecl {
    Utf8 { xml_ver: f32, standalone: bool },
    Utf8Ver1 { standalone: bool },
    Utf8Ver1Standalone,
    Ver1(&'static Encoding),
    Ver1Standalone(&'static Encoding),
    Ver11(&'static Encoding),
    Ver11Standalone(&'static Encoding),
    Custom {
        enc: &'static Encoding,
        xml_ver: f32,
        standalone: bool,
    },
    Empty
}

impl Display for XmlDecl {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let decl = match *self {
            Utf8 { xml_ver, standalone} => XmlDecl::xml_string(UTF_8, xml_ver, standalone),
            Utf8Ver1 { standalone } => XmlDecl::xml_string(UTF_8, 1.0, standalone ),
            Utf8Ver1Standalone => XmlDecl::xml_string(UTF_8, 1.0, true),
            Ver1(enc) => XmlDecl::xml_string(enc, 1.0, false),
            Ver1Standalone(enc) => XmlDecl::xml_string(enc, 1.0, true),
            Empty => String::new(),
            Custom {
                enc,
                xml_ver,
                standalone,
            } => XmlDecl::xml_string(enc, xml_ver, standalone),
            Ver11(enc) => XmlDecl::xml_string(enc, 1.1, false),
            Ver11Standalone(enc) => XmlDecl::xml_string(enc, 1.1, true),
        };
        write!(f, "{}", decl)
    }
}

impl XmlDecl {
    fn xml_string(enc: &'static Encoding, xml_ver: f32, standalone: bool) -> String {
        format!(
            "<?xml version=\"{:.1}\" encoding=\"{}\"{}?>",
            xml_ver,
            enc.name(),
            if standalone {
                " standalone=\"yes\""
            } else {
                ""
            }
        )
    }
}

pub fn decode_reader<R: Read>(
    reader: R,
    src_enc: &'static encoding_rs::Encoding,
    dest: &mut String,
) -> Result<usize, DeError> {
    let mut decoder = DecodeReaderBytesBuilder::new()
        .encoding(Some(src_enc))
        .build(reader);

    decoder
        .read_to_string(dest)
        .map_err(|e| DeError::Custom(e.to_string()))
}

pub fn decode_reader_to_string<R: Read>(
    reader: R,
    src_enc: &'static encoding_rs::Encoding,
) -> Result<String, DeError> {
    let mut result = String::new();

    decode_reader(reader, src_enc, &mut result).map(|_| result)
}

pub fn decode_from_reader<R: Read, D: DeserializeOwned>(
    reader: R,
    enc: &'static encoding_rs::Encoding,
) -> Result<D, DeError> {
    let decoder = DecodeReaderBytesBuilder::new()
        .encoding(Some(enc))
        .build(reader);

    de::from_reader(BufReader::new(decoder))
}

/// Try detect encoding
pub fn from_reader_decoded<R: Read, D: DeserializeOwned>(reader: &mut R) -> Result<D, DeError> {
    let mut bytes = Vec::new();

    loop {
        match reader.read_to_end(&mut bytes) {
            Ok(_) => break,
            Err(ref e) if e.kind() == ErrorKind::WouldBlock => continue,
            Err(ref e) if e.kind() == ErrorKind::Interrupted => continue,
            Err(e) => panic!("encountered IO error: {}", e),
        };
    };

    let mut enc_detector = EncodingDetector::new();

    if enc_detector.feed(&bytes, false) {
        let (str, _ ,_) = enc_detector.guess(None, false).decode(&bytes);
        de::from_str(&str)
    } else {
        Err(DeError::Custom(String::from("Err feed EncodingDetector with bytes from reader")))
    }

}

pub trait DeserializeXmlExt<D: DeserializeOwned> {
    fn decode_from_reader<R: Read>(reader: R, enc: &'static Encoding) -> Result<D, DeError>;

    fn from_reader_decoded<R: Read>(reader: &mut R) -> Result<D, DeError> {
        crate::from_reader_decoded(reader)
    }

    fn from_str(s: &str) -> Result<D, DeError> {
        de::from_reader(s.as_bytes())
    }

    fn from_reader<R: BufRead>(reader: R) -> Result<D, DeError> {
        de::from_reader(reader)
    }

}

impl<D: DeserializeOwned> DeserializeXmlExt<D> for D {
    fn decode_from_reader<R: Read>(reader: R, enc: &'static Encoding) -> Result<D, DeError> {
        crate::decode_from_reader(reader, enc)
    }
}

/// Description
///
/// Encode given *&str* with *enc* & write *&[u8]* result to dest Writer
///
/// * `src`  : source string
/// * `dest` : encoded bytes receiver
pub fn encode_str_to_writer<W: Write>(
    src: &str,
    writer: &mut W,
    enc: &'static Encoding,
) -> Result<(&'static Encoding, usize, bool), DeError> {
    let result = match enc.encode(src) {
        (Borrowed(v), e, err) => writer.write_all(v).map(|_| (e, v.len(), err)),
        (Owned(ref v), e, err) => writer.write_all(v).map(|_| (e, v.len(), err)),
    };

    result.map_err(|e| DeError::Custom(e.to_string()))
}

pub fn encode_to_writer<S: Serialize, O: Write>(
    src: &S,
    writer: &mut O,
    enc: &'static Encoding,
    decl: XmlDecl
) -> Result<(&'static Encoding, usize, bool), DeError> {
    let src = se::to_string(src)?;

    let src = format!("{}{}", decl, src);

    encode_str_to_writer(&src, writer, enc)
}

pub trait SerializeXmlExt {
    fn encode_to_writer<W: Write>(
        &self,
        writer: &mut W,
        enc: &'static Encoding,
        standalone: bool
    ) -> Result<(&'static Encoding, usize, bool), DeError>;

    fn encode_to_vec(&self, enc: &'static Encoding, standalone: bool) -> Result<(Vec<u8>, &'static Encoding, bool), DeError>;

    fn to_string_with_decl(&self, standalone: bool) -> Result<String, DeError>;

    fn to_writer_with_decl<W: Write>(&self, writer: W, standalone: bool) -> Result<(), DeError>;

    fn to_string(&self) -> Result<String, DeError>;

    fn to_writer<W: Write>(&self, writer: W) -> Result<(), DeError>;
}

impl<S: Serialize> SerializeXmlExt for S {

    /// Encode to given _Encoding_ string with _XmlDecl:Ver_ & >> _Write_
    /// - _xml_ver_: <?xml version="${xml_ver}"...
    /// - _standalone_: <?xml ... standalone="${standalone}"
    ///   if `false` { omitted from xml declaration } else { "yes" }
    fn encode_to_writer<W: Write>(
        &self,
        dest: &mut W,
        enc: &'static Encoding,
        standalone: bool
    ) -> Result<(&'static Encoding, usize, bool), DeError> {
        crate::encode_to_writer(self, dest, enc, if standalone { XmlDecl::Ver1Standalone(enc) } else { XmlDecl::Ver1(enc)})
    }

    fn encode_to_vec(
        &self,
        enc: &'static Encoding,
        standalone: bool,
    ) -> Result<(Vec<u8>, &'static Encoding, bool), DeError> {
        let mut buf = Vec::new();
        match S::encode_to_writer(&self, &mut buf, enc, standalone) {
            Ok(tpl) => {
                buf.shrink_to_fit();
                Ok((buf, tpl.0, tpl.2))
            }
            Err(err) => Err(err),
        }
    }

    /// Serialize to UTF-8 string with _XmlDecl_
    /// - _xml_ver_: <?xml version="${xml_ver}"...
    /// - _standalone_: <?xml ... standalone="${standalone}"
    ///   if `false` { omitted from xml declaration } else { "yes" }
    fn to_string_with_decl(&self, standalone: bool) -> Result<String, DeError> {
        se::to_string(self).map(|s| format!("{}{}", XmlDecl::Utf8Ver1 { standalone }, s))
    }

    /// Serialize to _String_ with _XmlDecl_ |> _Write_
    /// - _xml_ver_: <?xml version="${xml_ver}"...
    /// - _standalone_: <?xml ... standalone="${standalone}"
    ///   if `false` { omitted from xml declaration } else { "yes" }
    fn to_writer_with_decl<W: Write>(&self, mut writer: W, standalone: bool) -> Result<(), DeError> {
        let str = self.to_string_with_decl( standalone)?;
        writer
            .write_all(str.as_bytes())
            .map_err(|e| DeError::Custom(e.to_string()))
    }

    /// Serialize struct into a `String`
    fn to_string(&self) -> Result<String, DeError> {
        se::to_string(self)
    }

    /// Serialize struct into a `Write`r
    fn to_writer<W: Write>(&self, writer: W) -> Result<(), DeError> {
        se::to_writer(writer, self)
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
