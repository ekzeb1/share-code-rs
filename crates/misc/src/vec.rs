use std::ops::{Add, Deref, DerefMut};

trait ReturnOneLiners<T> {
    fn extend_and_return(self, other: Self) -> Self;
    fn push_and_return(self, item: T) -> Self;
}

impl<T> ReturnOneLiners<T> for Vec<T> {
    #[inline]
    fn extend_and_return(mut self, other: Self) -> Self {
        self.extend(other);
        self
    }

    #[inline]
    fn push_and_return(mut self, item: T) -> Self {
        self.push(item);
        self
    }
}

#[repr(transparent)]
pub struct V<T>(Vec<T>);

impl<T> Deref for V<T> {
    type Target = Vec<T>;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl<T> DerefMut for V<T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

impl<T> Add<Vec<T>> for V<T> {
    type Output = Vec<T>;

    fn add(self, rhs: Vec<T>) -> Self::Output {
        self.0.extend_and_return(rhs)
    }
}
