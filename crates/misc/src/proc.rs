use libc::c_int;
use std::io::Error;
use std::io::Result;

use colored::*;

fn check_err<T: Ord + Default>(num: T) -> Result<T> {
    if num < T::default() {
        return Err(Error::last_os_error());
    }

    Ok(num)
}

pub fn fork() -> Result<i32> {
    check_err(unsafe { libc::fork() })
}

pub fn waitpid(pid: i32) -> Result<i32> {
    check_err(unsafe { libc::waitpid(pid, std::ptr::null_mut::<c_int>(), 0) })
}

pub fn pre_fork(childs_num: usize, mut run_fn: impl FnMut()) -> Result<()> {
    let pid = std::process::id();
    let mut pids = Vec::new();

    for _ in 0..childs_num {
        let child_pid = fork()?;
        if child_pid == 0 {
            run_fn();
            break;
        } else {
            println!(
                "[{}] forking process, new {}",
                pid.to_string().bold().blue(),
                child_pid.to_string().yellow()
            );
        }

        pids.push(child_pid);
    }

    for p in pids {
        waitpid(p)?;
        eprintln!("[{p}] exited");
    }

    Ok(())
}
