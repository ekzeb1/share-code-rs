use std::time::Duration;

/// Conversion into a `Duration`.
pub trait IntoDuration {
    /// Creates a microsecond `Duration` from a Self.
    fn micros(self) -> Duration;

    /// Creates a millisecond `Duration` from a Self.
    fn millis(self) -> Duration;

    /// Creates a nanosecond `Duration` from a Self.
    fn nanos(self) -> Duration;

    /// Creates a second `Duration` from a Self.
    fn secs(self) -> Duration;

    /// Creates a minutes `Duration` from a Self.
    fn minutes(self) -> Duration;

    /// Creates a hours `Duration` from a Self.
    fn hours(self) -> Duration;
}

macro_rules! duration_impl {
    ($($selft:ty),*) => {
        $(
            impl IntoDuration for $selft {
                fn micros(self) -> std::time::Duration {
                    std::time::Duration::from_micros(self as u64)
                }
                fn millis(self) -> std::time::Duration {
                    std::time::Duration::from_millis(self as u64)
                }
                fn nanos(self) -> std::time::Duration {
                    std::time::Duration::from_nanos(self as u64)
                }
                fn secs(self) -> std::time::Duration {
                    std::time::Duration::from_secs(self as u64)
                }
                fn minutes(self) -> std::time::Duration {
                    std::time::Duration::from_secs((self * 60) as u64)
                }
                fn hours(self) -> std::time::Duration {
                    std::time::Duration::from_secs((self * 60 * 60) as u64)
                }
            }
        )*
    }
}

duration_impl!(u8, u16, u32, u64, u128, usize);
duration_impl!(i8, i16, i32, i64, i128, isize);
