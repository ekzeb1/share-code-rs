use std::fmt::{Display, Formatter};
use std::sync::mpsc;
use std::sync::{Arc, Mutex};
use std::thread;

type Job = Box<dyn FnOnce() + Send + 'static>;

enum Task {
    New(Job),
    Exit,
}

struct Worker {
    id: usize,
    handle: Option<thread::JoinHandle<()>>,
}

impl Display for Worker {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!("Job(id: {}, ..)", self.id))
    }
}

impl Worker {
    fn new(id: usize, receiver: Arc<Mutex<mpsc::Receiver<Task>>>) -> Self {
        let handle = thread::spawn(move || loop {
            let task = {
                let rx = receiver.lock().unwrap();
                rx.recv()
                    .unwrap_or_else(|_| panic!("Error recv on worker[{}] receiver", id))
            };

            match task {
                Task::New(job) => job(),
                Task::Exit => break,
            }
        });

        Worker {
            id,
            handle: Some(handle),
        }
    }
}

pub struct ThreadPool {
    workers: Vec<Worker>,
    sender: mpsc::Sender<Task>,
}

impl ThreadPool {
    pub fn with_capacity(size: usize) -> Self {
        let mut workers = Vec::with_capacity(size);

        let (sender, recv) = mpsc::channel();
        let recv = Arc::new(Mutex::new(recv));

        for id in 0..size {
            workers.push(Worker::new(id, Arc::clone(&recv)));
        }

        ThreadPool { workers, sender }
    }

    pub fn new() -> Self {
        let par = thread::available_parallelism()
            .expect("Error init ThreadPool: thread::available_parallelism Err")
            .get();

        ThreadPool::with_capacity(par)
    }

    pub fn execute<J>(&self, job: J)
    where
        J: FnOnce() + Send + 'static,
    {
        let job = Box::new(job);
        self.sender
            .send(Task::New(job))
            .expect("Error send Task::new(Job) to thread_pool");
    }
}

impl Drop for ThreadPool {
    fn drop(&mut self) {
        for _ in &self.workers {
            self.sender.send(Task::Exit).expect("Error send Task::Exit")
        }
        for worker in self.workers.iter_mut() {
            if let Some(handle) = worker.handle.take() {
                handle.join().expect("Error join worker.handler.thread")
            }
        }
    }
}

impl Default for ThreadPool {
    fn default() -> Self {
        ThreadPool::new()
    }
}
